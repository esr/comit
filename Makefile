# Makefile for the COMIT project
#
# The project logo was made with https://www.myfonts.com/fonts/typodermic/minicomputer/bold/?refby=whatfontis
#
# SPDX-FileCopyrightText: (C) Eric S. Raymond <esr@thyrsus.com>
# SPDX-License-Identifier: BSD-2-Clause

VERS=$(shell sed -n <NEWS.adoc '/^[0-9]/s/:.*//p' | head -1)

SOURCES = README.adoc COPYING NEWS.adoc TODO.adoc Makefile comit control *.adoc tests images comit-logo.png

all: comit comit.1 comit.html comit-paper.html comit-manual.html

clean:
	rm -f *~ *.1 *.html docs/*.html test/*~

.SUFFIXES: .html .adoc .1

# Note: to suppress the footers with timestamps being generated in HTML,
# we use "-a nofooter".
# To debug asciidoc problems, you may need to run "xmllint --nonet --noout --valid"
# on the intermediate XML that throws an error.
.SUFFIXES: .html .adoc .1

.adoc.1:
	asciidoctor -D. -a nofooter -b manpage $<
.adoc.html:
	asciidoctor -D. -a nofooter -a webfonts! $<

check:
	cd tests; make --quiet

pylint:
	@pylint --score=n comit

reflow:
	@black comit

version:
	@echo $(VERS)

comit-$(VERS).tar.gz: $(SOURCES) comit.1
	tar --transform='s:^:comit-$(VERS)/:' --show-transformed-names -cvzf comit-$(VERS).tar.gz $(SOURCES) comit.1

dist: comit-$(VERS).tar.gz

release: comit-$(VERS).tar.gz comit.html comit-paper.html comit-manual.html
	shipper version=$(VERS) | sh -e -x

refresh: comit.html
	shipper -N -w version=$(VERS) | sh -e -x
